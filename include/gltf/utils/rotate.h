#pragma once
#include <gltf/gvec.h>
#include <complex>
namespace gltf::utils
{

template<typename T>
using quaternion = gvec<T, 4>;

template<typename T>
constexpr quaternion<T> noRotation = {T(0), T(0), T(0), T(1)};

template<typename T>
constexpr quaternion<T> quaternionFrom(gvec<T, 3> const axle, T const angle) noexcept
{
    return {axle * std::sin(angle / T(2)), std::cos(angle / T(2))};
}
template<typename T>
constexpr quaternion<T> conjugate(quaternion<T> const &q) noexcept {return {-init(q), last(q)};}

template<typename T>
constexpr quaternion<T> compose(quaternion<T> const &q1, quaternion<T> const &q2) noexcept
{
    gvec<T, 3> const a1 = init(q1);
    gvec<T, 3> const a2 = init(q2);
    T const s1 = last(q1);
    T const s2 = last(q2);
    return {s1 * a2 + s2 * a1 + cross(a1, a2), s1 * s2 - dot(a1, a2)};
}

template<typename T>
constexpr quaternion<T> pow(quaternion<T> const &q, T const &t) noexcept
{
    T const cosTheta = last(q);
    T const sinTheta = std::max(T(0), std::sqrt(T(1) - cosTheta * cosTheta));
    if(sinTheta == T(0))
        return q;

    auto const z0 = std::complex<T>(cosTheta, sinTheta);
    auto const z = std::polar(T(1), t * std::arg(z0));

    return {init(q) * (z.imag() / z0.imag()), z.real()};
}
template<typename T>
constexpr quaternion<T> slerp(quaternion<T> const &q1, quaternion<T> const &q2, T const &t) noexcept
{
    return compose(pow(compose(q2, conjugate(q1)), t), q1);
}

template<typename T>
constexpr gmat<T, 3, 3> rotationFrom(quaternion<T> const q) noexcept
{
    return
    {
        {T(1) - T(2) * (q.y * q.y + q.z * q.z),        T(2) * (q.x * q.y + q.z * q.w),        T(2) * (q.x * q.z - q.y * q.w)},
        {       T(2) * (q.x * q.y - q.z * q.w), T(1) - T(2) * (q.x * q.x + q.z * q.z),        T(2) * (q.y * q.z + q.x * q.w)},
        {       T(2) * (q.x * q.z + q.y * q.w),        T(2) * (q.y * q.z - q.x * q.w), T(1) - T(2) * (q.x * q.x + q.y * q.y)},
    };
}
template<typename T>
constexpr quaternion<T> quaternionFrom(gmat<T, 3, 3> const m) noexcept
{
    // assert(determinant(m) == T(1));
    auto const diff = m - transpose(m);
    auto const w = T(0.5) * std::sqrt(std::max(T(0), T(1) + m.x.x + m.y.y + m.z.z));
    gvec<T, 3> const v = {diff.y.z, diff.z.x, diff.x.y};
    return {v / (T(4) * w), w};
}

template<typename T>
constexpr gvec<T, 3> rotate(quaternion<T> const q, gvec<T, 3> const v) noexcept
{
    return rotationFrom(q) * v;
}

} // namespace gltf::utils
