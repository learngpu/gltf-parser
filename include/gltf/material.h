#pragma once
#include <gltf/types.h>

namespace gltf
{

struct Material
{
    struct TextureInfo
    {
        u32 index;
        u32 texCoord = 0u;

        TextureInfo(gltf::json &&) noexcept;
    };
    struct NormalTextureInfo
    {
        u32 index;
        u32 texCoord = 0u;
        f32 scale = 1.f;

        NormalTextureInfo(gltf::json &&) noexcept;
    };
    struct OcclusionTextureInfo
    {
        u32 index;
        u32 texCoord = 0u;
        f32 strength = 1.f;

        OcclusionTextureInfo(gltf::json &&) noexcept;
    };
    struct PBRMetallicRoughness
    {
        vec4 baseColorFactor = {1.f, 1.f, 1.f, 1.f};
        std::optional<TextureInfo> baseColorTexture;
        f32 metallicFactor = 1.f;
        f32 roughnessFactor = 1.f;
        std::optional<TextureInfo> metallicRoughnessTexture;

        PBRMetallicRoughness(gltf::json &&) noexcept;
    };
    struct Extensions
    {
        struct KHR_MaterialsEmissiveStrength
        {
            f32 emissiveStrength = 1.f;

            KHR_MaterialsEmissiveStrength(gltf::json &&) noexcept;
        };
        struct KHR_MaterialsTransmission
        {
            f32 transmissionFactor = 0.f;
            std::optional<TextureInfo> transmissionTexture;

            KHR_MaterialsTransmission(gltf::json &&) noexcept;
        };
        struct KHR_MaterialsIOR
        {
            f32 ior = 1.5f;

            KHR_MaterialsIOR(gltf::json &&) noexcept;
        };
        struct KHR_MaterialsVolume
        {
            f32 thicknessFactor = 0.f;
            std::optional<TextureInfo> thicknessTexture;
            f32 attenuationDistance = 1.f / 0.f;
            vec3 attenuationColor = {1.f, 1.f, 1.f};

            KHR_MaterialsVolume(gltf::json &&) noexcept;
        };
        std::optional<KHR_MaterialsEmissiveStrength> KHR_materials_emissive_strength;
        std::optional<KHR_MaterialsTransmission    > KHR_materials_transmission;
        std::optional<KHR_MaterialsIOR             > KHR_materials_ior;
        std::optional<KHR_MaterialsVolume          > KHR_materials_volume;

        Extensions(gltf::json &&) noexcept;
    };

    std::string_view name;

    std::optional<          Extensions>           extensions;
    std::optional<PBRMetallicRoughness> pbrMetallicRoughness;
    std::optional<   NormalTextureInfo>        normalTexture;
    std::optional<OcclusionTextureInfo>     occlusionTexture;
    std::optional<         TextureInfo>      emissiveTexture;

    vec3 emissiveFactor = {0.f, 0.f, 0.f};

    enum class AlphaMode : u32
    {
        Opaque  = 0u,
        Mask    = 1u,
        Blend   = 2u,
    } alphaMode = AlphaMode::Opaque;
    f32 alphaCutoff = 0.5f;

    bool doubleSided = false;

    Material(gltf::json &&) noexcept;
};

} // namespace gltf
